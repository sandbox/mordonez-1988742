<?php
/**
 * @file
 * dc_cod_front_page.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function dc_cod_front_page_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'frontpage';
  $context->description = 'frontpage blocks on frontpage';
  $context->tag = 'frontpage';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-5fe9be24354c2a5b0c5da703cdaff3bd' => array(
          'module' => 'views',
          'delta' => '5fe9be24354c2a5b0c5da703cdaff3bd',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('frontpage');
  t('frontpage blocks on frontpage');
  $export['frontpage'] = $context;

  return $export;
}
