<?php
/**
 * @file
 * dc_cod_events.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dc_cod_events_user_default_permissions() {
  $permissions = array();

  // Exported permission: create paid_event registration.
  $permissions['create paid_event registration'] = array(
    'name' => 'create paid_event registration',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: update own paid_event registration.
  $permissions['update own paid_event registration'] = array(
    'name' => 'update own paid_event registration',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'registration',
  );

  // Exported permission: view own paid_event registration.
  $permissions['view own paid_event registration'] = array(
    'name' => 'view own paid_event registration',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'registration',
  );

  return $permissions;
}
