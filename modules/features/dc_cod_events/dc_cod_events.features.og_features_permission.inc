<?php
/**
 * @file
 * dc_cod_events.features.og_features_permission.inc
 */

/**
 * Implements hook_og_features_default_permissions().
 */
function dc_cod_events_og_features_default_permissions() {
  $permissions = array();

  // Exported og permission: 'node:event:update body field'
  $permissions['node:event:update body field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_experience field'
  $permissions['node:event:update field_experience field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_profile_first field'
  $permissions['node:event:update field_profile_first field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_profile_interests field'
  $permissions['node:event:update field_profile_interests field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_profile_job_title field'
  $permissions['node:event:update field_profile_job_title field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_profile_last field'
  $permissions['node:event:update field_profile_last field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_profile_org field'
  $permissions['node:event:update field_profile_org field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_session_track field'
  $permissions['node:event:update field_session_track field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_slides field'
  $permissions['node:event:update field_slides field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update field_speakers field'
  $permissions['node:event:update field_speakers field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:update og_user_node field'
  $permissions['node:event:update og_user_node field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:event:update own session content'
  $permissions['node:event:update own session content'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view body field'
  $permissions['node:event:view body field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
      'member' => 'member',
      'non-member' => 'non-member',
    ),
  );

  // Exported og permission: 'node:event:view field_event_product field'
  $permissions['node:event:view field_event_product field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_experience field'
  $permissions['node:event:view field_experience field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_paid_event field'
  $permissions['node:event:view field_paid_event field'] = array(
    'roles' => array(),
  );

  // Exported og permission: 'node:event:view field_profile_first field'
  $permissions['node:event:view field_profile_first field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_profile_interests field'
  $permissions['node:event:view field_profile_interests field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_profile_job_title field'
  $permissions['node:event:view field_profile_job_title field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_profile_last field'
  $permissions['node:event:view field_profile_last field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_profile_org field'
  $permissions['node:event:view field_profile_org field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_registration field'
  $permissions['node:event:view field_registration field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:view field_session_track field'
  $permissions['node:event:view field_session_track field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_slides field'
  $permissions['node:event:view field_slides field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view field_speakers field'
  $permissions['node:event:view field_speakers field'] = array(
    'roles' => array(
      'member' => 'member',
    ),
  );

  // Exported og permission: 'node:event:view group_group field'
  $permissions['node:event:view group_group field'] = array(
    'roles' => array(
      'administrator member' => 'administrator member',
    ),
  );

  // Exported og permission: 'node:event:view og_user_node field'
  $permissions['node:event:view og_user_node field'] = array(
    'roles' => array(),
  );

  return $permissions;
}
