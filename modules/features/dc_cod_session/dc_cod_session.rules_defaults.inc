<?php
/**
 * @file
 * dc_cod_session.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function dc_cod_session_default_rules_configuration() {
  $items = array();
  $items['rules_dc_cod_assign_main_og_session'] = entity_import('rules_config', '{ "rules_dc_cod_assign_main_og_session" : {
      "LABEL" : "Automatically assign main OG group after creation a session",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "og" ],
      "ON" : [ "node_insert" ],
      "IF" : [
        { "node_is_of_type" : { "node" : [ "node" ], "type" : { "value" : { "session" : "session" } } } }
      ],
      "DO" : [
        { "entity_fetch" : {
            "USING" : { "type" : "node", "id" : "1" },
            "PROVIDE" : { "entity_fetched" : { "event" : "Main event" } }
          }
        },
        { "og_group_content_add" : { "entity" : [ "node" ], "group" : [ "event" ] } }
      ]
    }
  }');
  return $items;
}
