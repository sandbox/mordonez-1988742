<?php
/**
 * @file
 * dc_cod_sitewide.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function dc_cod_sitewide_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = 'General context of all pages.';
  $context->tag = 'sitewide';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-dc_cod_sponsors-block' => array(
          'module' => 'views',
          'delta' => 'dc_cod_sponsors-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-dc_cod_community-block' => array(
          'module' => 'views',
          'delta' => 'dc_cod_community-block',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('General context of all pages.');
  t('sitewide');
  $export['sitewide'] = $context;

  return $export;
}
