<?php
/**
 * @file
 * dc_cod_sitewide.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dc_cod_sitewide_user_default_permissions() {
  $permissions = array();

  // Exported permission: access checkout.
  $permissions['access checkout'] = array(
    'name' => 'access checkout',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'commerce_checkout',
  );

  return $permissions;
}
