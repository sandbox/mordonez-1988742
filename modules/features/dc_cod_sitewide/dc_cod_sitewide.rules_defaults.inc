<?php
/**
 * @file
 * dc_cod_sitewide.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function dc_cod_sitewide_default_rules_configuration() {
  $items = array();
  $items['rules_dc_cod_assign_og_registration'] = entity_import('rules_config', '{ "rules_dc_cod_assign_og_registration" : {
      "LABEL" : "Automatically assign main OG group on registration",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_insert" ],
      "DO" : [
        { "data_set" : { "data" : [ "account:og-user-node" ], "value" : { "value" : [ "1" ] } } }
      ]
    }
  }');
  return $items;
}
