<?php

function dc_cod_install_tasks() {

  //make sure we have more memory than 196M. if not lets try to increase it.
  if (ini_get('memory_limit') != '-1' && ini_get('memory_limit') <= '196M') {
    ini_set('memory_limit', '196M');
  }

  return array(
    'dc_cod_create_first_group' => array(
      'display_name' => st('Create your first event'),
      'display' => TRUE,
      'type' => 'form',
    ),
    'dc_cod_create_sponsorship_levels' => array(
      'display_name' => st('Define sponsorship levels'),
      'display' => TRUE,
      'type' => 'form',
    ),
    'dc_cod_create_session_tracks' => array(
      'display_name' => st('Define tracks'),
      'display' => TRUE,
      'type' => 'form',
    ),
  );
}

/**
 * Implements hook_form_alter().
 * Set COD as the default profile.
 * (copied from Atrium: We use system_form_form_id_alter, otherwise we cannot alter forms.)
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach ($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'dc_cod';
  }
}

/**
 * Let the admin user create the first group as part of the installation process
 */
function dc_cod_create_first_group() {
  $form['cod_first_group_explanation'] = array(
    '#markup' => '<h2>' . st('Create your first event.') . '</h2>' . st("COD allows you to create multiple events and organizes content related to a particular event, such as sessions, attendees, and announcements."),
    '#weight' => -1,
  );

  $form['cod_first_group_title'] = array(
    '#type' => 'textfield',
    '#title' => st("Event name"),
    '#description' => st('This is the name of your conference or event. You may want to include the year or date if this is a repeating event.'),
    '#required' => TRUE,
    '#default_value' => st('DrupalCamp Spain'),
  );

  $form['cod_first_group_sku'] = array(
    '#type' => 'textfield',
    '#title' => st("SKU"),
    '#description' => st('Supply a unique identifier using letters, numbers, hyphens, and underscores. Commas may not be used.'),
    '#required' => TRUE,
    '#default_value' => st('dccod'),
  );

  $form['cod_first_group_price'] = array(
    '#type' => 'textfield',
    '#title' => st("Price"),
    '#required' => TRUE,
    '#default_value' => st('5'),
  );

  $form['cod_first_group_body'] = array(
    '#type' => 'textarea',
    '#title' => st('Event description'),
    '#description' => st("This text will appear on the event's homepage and gives a general overview of your event. You can always change this text later."),
    '#required' => TRUE,
    '#default_value' => st('DrupalCamp Spain is held once a year at the some place in Spain, with a regular attendance of around 300 Drupal developers.'),
  );

  $form['cod_first_group_explanation_registration'] = array(
    '#markup' => '<h3>' . st('Registration settings.') . '</h3>',
  );

  $form['cod_first_group_registration_capacity'] = array(
    '#type' => 'textfield',
    '#title' => st("Capacity"),
    '#description' => st("The maximum number of registrants. Leave at 0 for no limit."),
    '#required' => TRUE,
    '#default_value' => st('300'),
  );

  $form['cod_first_group_registration_from_address'] = array(
    '#type' => 'textfield',
    '#title' => st("From address"),
    '#description' => st("From email address to use for confirmations, reminders, and broadcast emails."),
    '#required' => TRUE,
    '#default_value' => variable_get('site_mail'),
  );

  $form['cod_first_group_submit'] = array(
    '#type'  => 'submit',
    '#value' => st('Save and continue'),
  );

  return $form;
}

/**
 * Let the admin user create the terms of sponsors.
 */
function dc_cod_create_sponsorship_levels() {
  $form['dc_cod_sponsorship_explanation'] = array(
    '#markup' => '<h2>' . st('Create your default terms') . '</h2>',
    '#weight' => -1,
  );

  $form['dc_cod_sponsorship_levels'] = array(
    '#type' => 'textarea',
    '#title' => st('Sponsorship levels'),
    '#description' => st("Put each sponsor separated by commas. No HTML allowed."),
    '#required' => TRUE,
    '#default_value' => st('Premium, Standard, Collaborators, Media Partners'),
  );

  $form['dc_cod_sponsorship_submit'] = array(
    '#type'  => 'submit',
    '#value' => st('Save and continue')
  );

  return $form;
}

/**
 * Let the admin user create the default session tracks.
 */
function dc_cod_create_session_tracks() {
  $form['dc_cod_session_tracks_explanation'] = array(
    '#markup' => '<h2>' . st('Create your default session tracks') . '</h2>',
    '#weight' => -1,
  );

  $form['dc_cod_session_tracks'] = array(
    '#type' => 'textarea',
    '#title' => st('Tracks'),
    '#description' => st("Put each session track separated by commas. No HTML allowed."),
    '#required' => TRUE,
    '#default_value' => st('Development & Performance, Site Building, Theming & Design & Usability'),
  );

  $form['dc_cod_session_tracks_submit'] = array(
    '#type'  => 'submit',
    '#value' => st('Save and continue')
  );

  return $form;
}

/**
 * Save terms for sponsorship levels.
 */
function dc_cod_create_sponsorship_levels_submit($form_id, &$form_state) {
  $values = $form_state['values'];
  _dc_cod_save_taxonomy_terms('sponsorship_level', explode(',', $values['dc_cod_sponsorship_levels']));
}

/**
 * Save terms for session tracks.
 */
function dc_cod_create_session_tracks_submit($form_id, &$form_state) {
  $values = $form_state['values'];
  _dc_cod_save_taxonomy_terms('session_tracks', explode(',', $values['dc_cod_session_tracks']));
}

/**
 * Helper function to save taxonomy terms on a vocabulary.
 */
function _dc_cod_save_taxonomy_terms($vocabulary_machine, $names) {

  if (empty($vocabulary_machine) || empty($names)) {
    return;
  }

  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine);

  foreach($names as $name) {
    $term = new StdClass();
    $term->name = trim($name);
    $term->vid = $vocabulary->vid;
    taxonomy_term_save($term);
  }
}

/**
 * Save the first group form.
 *
 * see commons_create_first_group().
 */
function dc_cod_create_first_group_submit($form_id, &$form_state) {
  $values = $form_state['values'];

  $form_state_product = array();
  $form_state_product['values'] = array();
  $form_product = array();
  $form_product['#parents'] = array();

  // Generate a new paid event.
  $product_paid_event = commerce_product_new('cod_paid_event');
  $product_paid_event->status = TRUE;
  $product_paid_event->uid = 1;
  $product_paid_event->sku = $values['cod_first_group_sku'];
  $product_paid_event->title = $values['cod_first_group_title'];
  $product_paid_event->created = $product_paid_event->changed = time();
  $price = array(
    LANGUAGE_NONE => array(
      0 => array(
        'amount' => $values['cod_first_group_price'] * 100,
        'currency_code' => commerce_default_currency(),
      ),
    ),
  );
  $form_state_product['values']['commerce_price'] = $price;
  field_attach_submit('commerce_product', $product_paid_event, $form_product, $form_state_product);
  commerce_product_save($product_paid_event);

  $event = new stdClass();
  $event->type = 'event';
  node_object_prepare($event);

  $event->title = $values['cod_first_group_title'];
  $event->body[LANGUAGE_NONE][0]['value'] = $values['cod_first_group_body'];
  $event->uid = 1;
  $event->language = LANGUAGE_NONE;
  $event->status = 1;
  $event->field_event_product['und'][0]['product_id'] = $product_paid_event->product_id;
  $event->field_paid_event['und'][0]['value'] = 1;
  node_save($event);

  // Attach the registration.
  _dc_cod_attach_registration($event, $product_paid_event, $values);

  _dc_cod_create_menu_items($event);

  drupal_flush_all_caches();
}

/**
 * Attach a registration settings to the event.
 */
function _dc_cod_attach_registration(&$event, &$product_paid_event, $values) {
  $registration = array(
    'capacity' => $values['cod_first_group_registration_capacity'],
    'scheduling' => array(
      'open' => '',
      'close' => '',
    ),
    'reminder' => array(
      'send_reminder' => 0,
      'reminder_settings' => array(
        'reminder_date' => '',
        'reminder_template' => '',
      ),
    ),
    'settings' => array(
      'multiple_slots' => 0,
      'multiple_registrations' => 1,
      'from_address' => $values['cod_first_group_registration_from_address'],
      'hide_from_display' => 1,
    ),
    'reg_status' => 1,
    'status' => 1,
  );

  $registration_settings = registration_convert_form_settings($registration);
  registration_update_entity_settings('commerce_product', $product_paid_event->product_id, $registration_settings);

  // Update registration product display settings.
  // See commerce_registration_node_settings_form_submit.
  db_merge('commerce_registration_node_settings')
    ->key(array('nid' => $event->nid))
    ->fields(array(
      'settings' => serialize($registration['settings']),
    ))
    ->execute();
}

/**
 * Create menu items in main and secondary menus.
 */
function _dc_cod_create_menu_items($event) {
  $items = array(
    array(
      'link_path' => '<front>',
      'link_title' => 'Home',
      'menu_name' => 'main-menu',
      'weight' => '-50',
    ),
    array(
      'link_path' => 'node/' . $event->nid . '/program/sessions/proposed',
      'link_title' => 'Proposed Sessions',
      'menu_name' => 'main-menu',
      'weight' => '-50',
    ),
    array(
      'link_path' => 'user/register',
      'link_title' => 'Register',
      'menu_name' => 'main-menu',
      'weight' => 50,
    ),
    array(
      'link_path' => 'node/add/session',
      'link_title' => 'Submit Session',
      'menu_name' => 'user-menu',
      'weight' => 0,
    ),
  );

  foreach ($items as $item) {
    menu_link_save($item);
  }
}